package com.squeakyfoxx.yui;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Yui extends Game {

	private SpriteBatch batch;
//	private Texture letterboxTest;

	private Viewport viewport;
	private OrthographicCamera camera;

	private TiledMapRenderer tiledMapRenderer;
	private TiledMap map;

	@Override
	public void create () {

		// setup camera and viewport to view the game world
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 256, 144);
		viewport = new FitViewport(256, 144, camera);

		// setup map load test map and setup map renderer
		map = new TmxMapLoader().load("load-test.tmx");
		tiledMapRenderer = new OrthogonalTiledMapRenderer(map, 1/16f);
		batch = new SpriteBatch();
//		letterboxTest = new Texture(Gdx.files.internal("letterbox-test.png"));
	}

	@Override
	public void render () {
		ScreenUtils.clear(0, 0, 0, 0);

		batch.setProjectionMatrix(camera.combined);

		batch.begin();
//			batch.draw(letterboxTest, 0, 0);
		batch.end();
	}

	@Override
	public void resize(int width, int height){
		viewport.update(width, height);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
//		letterboxTest.dispose();
		map.dispose();
	}
}
